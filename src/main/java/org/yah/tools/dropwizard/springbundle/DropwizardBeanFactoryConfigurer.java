package org.yah.tools.dropwizard.springbundle;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;

import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;


public interface DropwizardBeanFactoryConfigurer<T extends Configuration> {
	void configure(ConfigurableBeanFactory beanFactory, T configuration, Environment environment);
}
