package org.yah.tools.dropwizard.springbundle.filter;

import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.OptionalInt;
import java.util.Set;

import javax.annotation.Priority;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationUtils;

import io.dropwizard.jetty.setup.ServletEnvironment;

public class FilterRegistration implements Ordered {

	private final Filter filter;

	private final String name;

	private final EnumSet<DispatcherType> dispatcherTypes;

	private final String[] urlPatterns;

	private final int order;

	private FilterRegistration(Builder builder) {
		this.filter = builder.filter;
		this.name = builder.name;
		this.dispatcherTypes = EnumSet.copyOf(builder.dispatcherTypes);
		this.urlPatterns = builder.urlPatterns.toArray(new String[builder.urlPatterns.size()]);
		this.order = builder.oder;
	}

	public void register(ServletEnvironment environment, String name) {
		environment.addFilter(this.name != null ? this.name : name, filter)
			.addMappingForUrlPatterns(dispatcherTypes, true, urlPatterns);
	}

	@Override
	public int getOrder() {
		return order;
	}

	public static Builder builder(Filter filter) {
		return new Builder(filter);
	}

	public static class Builder {

		private final Filter filter;

		private String name;

		private EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST);

		private Set<String> urlPatterns = Collections.emptySet();

		private int oder = Ordered.LOWEST_PRECEDENCE;

		private Builder(Filter filter) {
			this.filter = Objects.requireNonNull(filter, "filter is null");
			getFilterPriority(filter).ifPresent(this::withOrder);
		}

		private static OptionalInt getFilterPriority(Filter filter) {
			Priority annotation = AnnotationUtils.getAnnotation(filter.getClass(), Priority.class);
			if (annotation != null)
				return OptionalInt.of(annotation.value());
			return OptionalInt.empty();
		}

		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Builder withDispactherTypes(DispatcherType... dispatcherTypes) {
			this.dispatcherTypes = EnumSet.noneOf(DispatcherType.class);
			for (int i = 0; i < dispatcherTypes.length; i++) {
				this.dispatcherTypes.add(dispatcherTypes[i]);
			}
			return this;
		}

		public Builder withUrlPatterns(String... urlPatterns) {
			this.urlPatterns = new LinkedHashSet<>(urlPatterns.length);
			for (int i = 0; i < urlPatterns.length; i++) {
				this.urlPatterns.add(urlPatterns[i]);
			}
			return this;
		}

		public Builder withOrder(int order) {
			this.oder = order;
			return this;
		}

		public FilterRegistration build() {
			if (urlPatterns.isEmpty())
				urlPatterns = Collections.singleton("/*");
			return new FilterRegistration(this);
		}
	}
}
