/**
 * 
 */
package org.yah.tools.dropwizard.springbundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.yah.tools.dropwizard.springbundle.filter.FilterRegistration;

import io.dropwizard.Configuration;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;


/**
 * @author Yah
 *
 */
public class DropwizardApplicationContext<T extends Configuration> implements Managed {

	public static final String CONFIGURATION_BEAN_NAME = "dwConfiguration";

	public static final String ENVIRONMENT_BEAN_NAME = "dwEnvironment";

	private final T dropwizardConfiguration;

	private final Environment environment;

	private final Class<?>[] contextConfigurationClasses;

	private final DropwizardBeanFactoryConfigurer<T> configurer;

	private final ConfigurableApplicationContext context;

	DropwizardApplicationContext(T dropwizardConfiguration,
	        Environment environment,
	        DropwizardBeanFactoryConfigurer<T> configurer,
	        Class<?>[] contextConfigurationClasses) {
		this.dropwizardConfiguration = dropwizardConfiguration;
		this.environment = environment;
		this.contextConfigurationClasses = contextConfigurationClasses;
		this.configurer = configurer;
		this.context = createSpringContext();
	}

	@Override
	public void start() throws Exception {
		// context is already started (outside jetty lifecycle)
	}

	@Override
	public void stop() throws Exception {
		context.close();
	}

	private ConfigurableApplicationContext createSpringContext() {
		DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
		beanFactory.registerSingleton(CONFIGURATION_BEAN_NAME, dropwizardConfiguration);
		beanFactory.registerSingleton(ENVIRONMENT_BEAN_NAME, environment);
		configurer.configure(beanFactory, dropwizardConfiguration, environment);
		GenericApplicationContext dropwizardContext = new GenericApplicationContext(beanFactory);
		dropwizardContext.refresh();

		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
		applicationContext.setParent(dropwizardContext);
		applicationContext.register(contextConfigurationClasses);
		applicationContext.registerBean(DropwizardBeanPostProcessor.class,
		        () -> new DropwizardBeanPostProcessor(environment));
		applicationContext.refresh();

		List<Entry<String, FilterRegistration>> filterRegistrations = new ArrayList<>(
		        applicationContext.getBeansOfType(FilterRegistration.class).entrySet());
		Collections.sort(filterRegistrations, Comparator.comparingInt(e -> e.getValue().getOrder()));
		filterRegistrations.forEach(e -> e.getValue().register(environment.servlets(), e.getKey()));

		applicationContext.start();
		return applicationContext;
	}

}
