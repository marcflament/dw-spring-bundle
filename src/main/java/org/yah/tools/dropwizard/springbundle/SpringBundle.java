package org.yah.tools.dropwizard.springbundle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.dropwizard.Configuration;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;


public class SpringBundle<T extends Configuration> implements ConfiguredBundle<T> {

	private final DropwizardBeanFactoryConfigurer<T> configurer;

	private final Class<?>[] configurationClasses;

	private SpringBundle(List<Class<?>> configurationClasses, DropwizardBeanFactoryConfigurer<T> configurer) {
		this.configurationClasses = configurationClasses.toArray(new Class[configurationClasses.size()]);
		this.configurer = configurer;
	}

	@Override
	public void initialize(Bootstrap<?> bootstrap) {
		//no op
	}

	@Override
	public void run(T configuration, Environment environment) throws Exception {
		DropwizardApplicationContext<T> context = new DropwizardApplicationContext<>(configuration,
		        environment,
		        configurer,
		        configurationClasses);
		environment.lifecycle().manage(context);
	}

	public static <T extends Configuration> Builder<T> builder(Class<?>... configurations) {
		return new Builder<>(configurations);
	}

	public static final class Builder<T extends Configuration> {
		
		private final List<Class<?>> configurations = new ArrayList<>();
		
		private DropwizardBeanFactoryConfigurer<T> configurer = (bf, e, c) -> {};

		public Builder(Class<?>... configurations) {
			if (configurations.length == 0)
				throw new IllegalArgumentException("At least one configuration is expected");
			for (int i = 0; i < configurations.length; i++) {
				this.configurations.add(configurations[i]);
			}
		}

		public Builder<T> withConfiguration(Class<?> configuration) {
			configurations.add(Objects.requireNonNull(configuration, "configuration is null"));
			return this;
		}

		public Builder<T> withParentConfigurer(DropwizardBeanFactoryConfigurer<T> configurer) {
			this.configurer = Objects.requireNonNull(configurer, "configurer is null");
			return this;
		}

		public SpringBundle<T> build() {
			return new SpringBundle<>(configurations, configurer);
		}
	}
}
