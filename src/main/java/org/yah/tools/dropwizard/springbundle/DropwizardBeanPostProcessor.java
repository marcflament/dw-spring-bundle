/**
 * 
 */
package org.yah.tools.dropwizard.springbundle;

import java.lang.annotation.Annotation;

import javax.ws.rs.Path;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.ExceptionMapper;

import org.eclipse.jetty.util.component.LifeCycle;
import org.glassfish.hk2.api.DynamicConfiguration;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;

import com.codahale.metrics.health.HealthCheck;

import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;


/**
 * @author Yah
 */
public class DropwizardBeanPostProcessor implements BeanPostProcessor {

	private final Environment environment;

	DropwizardBeanPostProcessor(Environment environment) {
		this.environment = environment;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) {
		if (findAnnotation(bean, Path.class) != null)
			environment.jersey().register(bean);

		if (bean instanceof HealthCheck)
			environment.healthChecks().register(beanName, (HealthCheck) bean);

		if (bean instanceof Managed)
			environment.lifecycle().manage((Managed) bean);

		if (bean instanceof LifeCycle)
			environment.lifecycle().manage((LifeCycle) bean);

		if (bean instanceof ContextResolver)
			environment.jersey().register(bean);

		if (bean instanceof ExceptionMapper)
			environment.jersey().register(bean);

		if (bean instanceof DynamicFeature)
			environment.jersey().register(bean);

		if (bean instanceof DynamicConfiguration)
			environment.jersey().register(bean);

		return bean;
	}

	private static <T extends Annotation> T findAnnotation(Object bean, Class<T> annotationClass) {
		return AnnotationUtils.findAnnotation(bean.getClass(), annotationClass);
	}

}
